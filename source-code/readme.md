<h1>Results ( in ticks* ):</h1>

<h3>Test:</h3>
 - Greedy 1002
 - ACO 193
 - ACO ( without A* ) 155

<h3>Test1:</h3>
 - Greedy 315
 - ACO 31
 - ACO ( without A* ) 22
 
<h3>Test2:</h3>
 - Greedy 398
 - ACO 150
 - ACO ( without A* ) currently not finding path