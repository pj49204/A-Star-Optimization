package hr.fer.zemris;

import hr.fer.zemris.algorithms.ACOAlgorithm;
import hr.fer.zemris.algorithms.AStar;
import hr.fer.zemris.algorithms.PathfindingAlgorithm;
import hr.fer.zemris.entities.Player;
import hr.fer.zemris.graph.*;
import hr.fer.zemris.plots.BoxPlot;

import java.awt.*;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.*;
import java.util.List;
import java.util.stream.Collectors;

import static javafx.scene.input.KeyCode.F;

public class Main {
    public static void main(String[] args) {
        Main main = new Main();
        main.main(Integer.parseInt(args[0]));
    }

    private void test1() {
        try {
            ClassLoader classLoader = getClass().getClassLoader();
            File file = new File(classLoader.getResource("testGraph.txt").getFile());
            Graph graph = GraphCreator.parseFromFile(file);
            graph.visit(node -> {
                System.out.println(node.getLocation().toString());
                for (Edge edge : node.getEdges()) {
                    System.out.println("\t" + edge.getTo().toString());
                }
            });
            PathfindingAlgorithm algorithm = new AStar();
            System.out.println(Arrays.toString(algorithm.getPath(0, 0, 2, 2, graph).toArray()));
        } catch (IOException | NullPointerException e) {
            e.printStackTrace();
        }
    }

    private void test2() {
        try {
            Graph g = GraphRandomGenerator.generateRandomGraph(50, 50);
            File file = new File("test.txt");
            g.writeToFile(file);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void test3() {
        try {
            File file = new File("test2.txt");
            Graph graph = GraphCreator.parseFromFile(file);
            graph.visit(node -> {
                System.out.println(node.getLocation().toString());
                for (Edge edge : node.getEdges()) {
                    System.out.println("\t" + edge.getTo().toString());
                }
            });
            GraphVisualiser visualiser = new GraphVisualiser(graph);
            visualiser.visualize();
            PathfindingAlgorithm algorithm = new AStar();
            int x0, y0, x1, y1;
            Scanner scanner = new Scanner(System.in);
            while (scanner.hasNext()) {
                x0 = scanner.nextInt();
                y0 = scanner.nextInt();
                x1 = scanner.nextInt();
                y1 = scanner.nextInt();
                visualiser.visualisePath(algorithm.getPath(x0, y0, x1, y1, graph));
                visualiser.refresh();
            }
            scanner.close();

        } catch (IOException | NullPointerException e) {
            e.printStackTrace();
        }
    }

    private void test4(int i) {
        try {
            File graphFile = new File("test" + Integer.toString(i) + ".txt");
            File queryFile = new File("queries" + Integer.toString(i) + ".txt");


            Graph graph = GraphCreator.parseFromFile(graphFile);
            graph.visit(node -> {
                System.out.println(node.getLocation().toString());
                for (Edge edge : node.getEdges()) {
                    System.out.println("\t" + edge.getTo().toString());
                }
            });

            GraphVisualiser visualiser = new GraphVisualiser(graph);
            visualiser.visualize();

            Simulator simulator = new Simulator(graph);

            PathfindingAlgorithm baseAlgorithm = new AStar();
            PathfindingAlgorithm algorithm = new ACOAlgorithm(simulator, baseAlgorithm);
//            PathfindingAlgorithm algorithm = new ACOAlgorithm(simulator);
//            PathfindingAlgorithm algorithm = baseAlgorithm;


            Files.readAllLines(queryFile.toPath()).forEach(line -> {
                String[] data = line.split(" ");
                int x0, y0, x1, y1;
                x0 = Integer.parseInt(data[0]);
                y0 = Integer.parseInt(data[1]);
                x1 = Integer.parseInt(data[2]);
                y1 = Integer.parseInt(data[3]);
                List<Edge> path = algorithm.getPath(x0, y0, x1, y1, graph);
                Player player = new Player(new Node.Location(x0, y0));
                player.setName("REAL PLAYER");
                simulator.addPath(player, path);
                visualiser.visualisePath(path);
                visualiser.refresh();
                try {
                    Thread.sleep(50);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            });

            int ticks = 0;
            while (true) {
                try {
                    simulator.step();
                    ticks++;
                } catch (Simulator.NoMoreStepsException e) {
                    break;
                }
//                try {
//                    Thread.sleep(10);
//                } catch (InterruptedException e) {
//                    e.printStackTrace();
//                }
            }
            for (Player player : simulator.getPlayers()) {
                System.out.printf("%s: %f\n", player.getId(), player.getTotalDistance());
            }
            System.out.println(ticks);
            List<Number> numbers = simulator.getPlayers().stream().mapToDouble(Player::getTotalDistance).boxed().collect(Collectors.toList());
            System.out.println(numbers.toString());
            EventQueue.invokeLater(new BoxPlot(numbers)::display);

        } catch (IOException | NullPointerException e) {
            e.printStackTrace();
        }
    }

    private void test5(int i) {
        try {
            File graphFile = new File("test0.txt");
            File queryFile = new File("queries0.txt");


            Graph graph = GraphCreator.parseFromFile(graphFile);
            graph.visit(node -> {
                System.out.println(node.getLocation().toString());
                for (Edge edge : node.getEdges()) {
                    System.out.println("\t" + edge.getTo().toString());
                }
            });

            GraphVisualiser visualiser = new GraphVisualiser(graph);
            visualiser.visualize();

            Simulator simulator = new Simulator(graph);

            PathfindingAlgorithm baseAlgorithm = new AStar();
            PathfindingAlgorithm algorithm = new ACOAlgorithm(simulator, baseAlgorithm);
//            PathfindingAlgorithm algorithm = new ACOAlgorithm(simulator);
//            PathfindingAlgorithm algorithm = baseAlgorithm;


            Files.readAllLines(queryFile.toPath()).forEach(line -> {
                String[] data = line.split(" ");
                int x0, y0, x1, y1;
                x0 = Integer.parseInt(data[0]);
                y0 = Integer.parseInt(data[1]);
                x1 = Integer.parseInt(data[2]);
                y1 = Integer.parseInt(data[3]);
                List<Edge> path = algorithm.getPath(x0, y0, x1, y1, graph);
                Player player = new Player(new Node.Location(x0, y0));
                player.setName("REAL PLAYER");
                simulator.addPath(player, path);
                visualiser.visualisePath(path);
                visualiser.refresh();
            });

            int ticks = 0;
            while (true) {
                try {
                    simulator.step();
                    ticks++;
                    if (ticks == i) {
                        Map<Player, List<Edge>> paths = new HashMap<>();
                        for (Map.Entry<Player, List<Edge>> entry : simulator.getPaths().entrySet()) {
                            List<Edge> path = entry.getValue();
                            Player player = entry.getKey();
                            paths.put(player, path);
                        }
                        visualiser.removeAllPaths();
                        simulator.removeAllPaths();
                        Node from = graph.getNode(0, 0);
                        Node to = graph.getNode(1, 1);
                        List<Edge> fromEdges = new ArrayList<>();
                        List<Edge> toEdges = new ArrayList<>();
                        for (Edge edge : from.getEdges()) {
                            if (!edge.getTo().equals(to)) {
                                fromEdges.add(edge);
                            }
                        }
                        for (Edge edge : to.getEdges()) {
                            if (!edge.getTo().equals(from)) {
                                toEdges.add(edge);
                            }
                        }
                        from.setEdges(fromEdges);
                        to.setEdges(toEdges);


                        for (Map.Entry<Player, List<Edge>> entry : paths.entrySet()) {
                            List<Edge> path = entry.getValue();
                            Player player = entry.getKey();
                            if (!path.isEmpty()) {
                                Node.Location start = path.get(0).getFrom().getLocation();
                                Node.Location end = path.get(path.size() - 1).getTo().getLocation();
                                List<Edge> newPath = algorithm.getPath(start.x, start.y, end.x, end.y, graph);
                                simulator.addPath(player, newPath);
                                visualiser.visualisePath(newPath);
                                visualiser.refresh();
                            }
                        }
                    }
                } catch (Simulator.NoMoreStepsException e) {
                    break;
                }
//                try {
//                    Thread.sleep(10);
//                } catch (InterruptedException e) {
//                    e.printStackTrace();
//                }
            }
            for (Player player : simulator.getPlayers()) {
                System.out.printf("%s: %f\n", player.getId(), player.getTotalDistance());
            }
            System.out.println(ticks);
            List<Number> numbers = simulator.getPlayers().stream().mapToDouble(Player::getTotalDistance).boxed().collect(Collectors.toList());
            System.out.println(numbers.toString());
            EventQueue.invokeLater(new BoxPlot(numbers)::display);

        } catch (IOException | NullPointerException e) {
            e.printStackTrace();
        }
    }

    private void main(int test) {
        if (test == 3) {
            test3();
        } else if (test == 4) {
            test4(2);
        } else if (test == 5) {
            test5(60);
        }
    }

}
