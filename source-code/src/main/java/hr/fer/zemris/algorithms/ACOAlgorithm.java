package hr.fer.zemris.algorithms;

import hr.fer.zemris.entities.Player;
import hr.fer.zemris.graph.Edge;
import hr.fer.zemris.graph.Graph;
import hr.fer.zemris.graph.Node;
import hr.fer.zemris.graph.Simulator;
import sun.dc.pr.PRError;

import javax.xml.crypto.dom.DOMCryptoContext;
import java.nio.charset.StandardCharsets;
import java.nio.file.Path;
import java.security.SecureRandom;
import java.util.*;
import java.util.function.BiFunction;
import java.util.function.Function;

public class ACOAlgorithm implements PathfindingAlgorithm {

    private PathfindingAlgorithm basePathfindingAlgorithm;
    private Simulator simulator;
    private static final double EVAPORATION_RATE = 0.005;
    private static final double ALPHA = 1.2;
    private static final double BETA = 1.0;
    private static final double INITIAL_PHEROMONE_VALUE = 1.0;
    private static final double HEURISTIC_FACTOR = 15;
    private Map<Node, Double> pheromoneLevel;
    private static final Random random = new SecureRandom("Some seed".getBytes(StandardCharsets.UTF_8));
    private static final int ITERATION_COUNT = 1000;
    private static final int RESET_PERCENT = 10;
    private Map<Query, List<Edge>> pathCache = new HashMap<>();

    public ACOAlgorithm(Simulator simulator) {
        this(
                simulator,
                (x1, y1, x2, y2, graph) -> new ArrayList<>()
        );
    }

    public ACOAlgorithm(Simulator simulator, PathfindingAlgorithm basePathfindingAlgorithm) {
        this.basePathfindingAlgorithm = basePathfindingAlgorithm;
        this.simulator = simulator;
        this.pheromoneLevel = new HashMap<>();
        resetPheromones();

    }

    private void resetPheromones() {
        simulator.getGraph().getNodes().forEach((location, node) -> {
            pheromoneLevel.put(node, INITIAL_PHEROMONE_VALUE);
        });
    }

    private void setHeuristicPheromones(int x1, int y1, int x2, int y2, Graph graph) {
        List<Edge> path = getPathFromBase(new Query(x1, y1, x2, y2), graph);
        for (int i = 0, n = path.size(); i < n; i++) {
            Edge edge = path.get(i);
            pheromoneLevel.merge(edge.getTo(), ( ( i + 1.0 ) / (float)n ) * Math.pow(n, 0.7), (t, dt) -> t + dt);
        }
    }

    private void updatePheromones(List<Edge> path, double distance) {
        for (Map.Entry<Node, Double> entry : pheromoneLevel.entrySet()) {
            pheromoneLevel.merge(
                    entry.getKey(),
                    0.0,
                    (t, dt) -> (1 - EVAPORATION_RATE) * t
            );
        }
        for (Edge edge : path) {
            pheromoneLevel.merge(
                    edge.getTo(),
                    1.0 / (distance),
                    (t, dt) -> t + dt
            );
        }
    }

    @Override
    public List<Edge> getPath(int x1, int y1, int x2, int y2, Graph graph) {
        resetPheromones();
        setHeuristicPheromones(x1, y1, x2, y2, graph);
        List<Pair<List<Edge>, Double>> paths = new ArrayList<>();
        int sameSolutionCounter = 1;
        double bestSolutionValue = Double.MAX_VALUE;
        for (int i = 0; i < ITERATION_COUNT; i++) {
            Pair<List<Edge>, Double> path = getPathForAnt(x1, y1, x2, y2, simulator);
            updatePheromones(path.first, path.second);
            if (path.second > 0) {
                paths.add(path);
                if (path.second < bestSolutionValue) {
                    bestSolutionValue = path.second;
                    sameSolutionCounter = 1;
                } else {
                    sameSolutionCounter++;
                }
            }
            if (sameSolutionCounter >= ITERATION_COUNT * (RESET_PERCENT / 100.0)) {
                resetPheromones();
                setHeuristicPheromones(x1, y1, x2, y2, graph);
                sameSolutionCounter = 1;
            }
        }
        Pair<List<Edge>, Double> best = paths.stream()
                .min(Comparator.comparingDouble(o -> o.second))
                .orElseGet(() -> new Pair<>(Collections.emptyList(), 0.0));
        System.out.println(best.second.toString() + " " + best.first.size() + " " + best.first.toString());
        List<Edge> result = new ArrayList<>();
        for (Edge edge : best.first) {
            Node from = graph.getNode(edge.getFrom().getLocation().x, edge.getFrom().getLocation().y);
            Node to = graph.getNode(edge.getTo().getLocation().x, edge.getTo().getLocation().y);
            for (Edge edge1 : from.getEdges()) {
                if (edge1.getTo().equals(to)) {
                    result.add(edge1);
                    break;
                }
            }
        }
        if (result.isEmpty()) {
            result = getPathFromBase(new Query(x1, y1, x2, y2), graph);
        }
        return result;
    }

    private List<Edge> getPathFromBase(Query query, Graph graph) {
        if (!pathCache.containsKey(query)) {
            pathCache.put(query, basePathfindingAlgorithm.getPath(query.x0, query.y0, query.x1, query.y1, graph));
        }
        return pathCache.get(query);
    }

    private Pair<List<Edge>, Double> getPathForAnt(int x1, int y1, int x2, int y2, Simulator simulator) {
        simulator = simulator.getCopy();
        Player ant = new Player(new Node.Location(x1, y1));
        simulator.addPath(ant, Collections.emptyList());
        Graph graph = simulator.getGraph();
        Node source = graph.getNode(x1, y1);
        Node destination = graph.getNode(x2, y2);
        Stack<Node> open = new Stack<>();
        open.add(source);
        Set<Node> visited = new HashSet<>();
        List<Edge> path = new ArrayList<>();
        int ticks = 0;
        while (!open.isEmpty()) {
            Node current = open.pop();
            if (ant.getLocation().equals(destination.getLocation())) {
                return new Pair<>(path, (double) ticks);
            }
            visited.add(current);
            if (current.getLocation().equals(ant.getLocation())) { // arrived to next location
                Map<Edge, Double> probabilities = new HashMap<>();
                List<Edge> possibleEdges = new ArrayList<>();
                for (Edge edge : current.getEdges()) {
                    if (!visited.contains(edge.getTo())) {
                        possibleEdges.add(edge);
                    }
                }
                for (Edge edge : possibleEdges) {
                    probabilities.put(edge, getProbability(edge, possibleEdges, destination, graph));
                }
                double border = 0.0;
                double randomP = random.nextDouble();
                for (Map.Entry<Edge, Double> entry : probabilities.entrySet()) {
                    double probability = entry.getValue();
                    if (border <= randomP && randomP <= border + probability) {
                        open.add(entry.getKey().getTo());
                        simulator.addPath(ant, new ArrayList<>(Collections.singletonList(entry.getKey())));
                        path.add(entry.getKey());
                        break;
                    }
                    border += probability;
                }
            } else {
                open.add(current);
            }
            if (!open.empty()) {
                simulator.step();
            }
            ticks++;
        }
        return new Pair<>(path, -1.0 * path.size() * 1000);
    }

    private double getProbability(Edge edge, List<Edge> possibleEdges, Node destination, Graph graph) {
        BiFunction<Double, Double, Double> getWeight = (t, n) -> Math.pow(t, ALPHA) * Math.pow(n, BETA);
        double numerator = getWeight.apply(pheromoneLevel.get(edge.getTo()), getHeuristicDistance(edge.getFrom(), destination, graph));
        double denominator = possibleEdges.stream().mapToDouble(edge1 -> getWeight.apply(pheromoneLevel.get(edge1.getTo()), getHeuristicDistance(edge1.getFrom(), destination, graph))).sum();
        return denominator == 0 ? 0 : numerator / denominator;
    }

    private double getHeuristicDistance(Node from, Node to, Graph graph) {

        List<Edge> path = getPathFromBase(
                new Query(from.getLocation().x, from.getLocation().y, to.getLocation().x, to.getLocation().y),
                graph
        );
        return path.size() == 0 ? AStar.heuristicDistance(from, to) : path.size();
    }


    public static class Pair<T, U> {
        T first;
        U second;

        public Pair(T first, U second) {
            this.first = first;
            this.second = second;
        }

        public T getFirst() {
            return first;
        }

        public void setFirst(T first) {
            this.first = first;
        }

        public U getSecond() {
            return second;
        }

        public void setSecond(U second) {
            this.second = second;
        }
    }

    private static class Query {
        int x0;
        int y0;
        int x1;
        int y1;

        Query(int x0, int y0, int x1, int y1) {
            this.x0 = x0;
            this.y0 = y0;
            this.x1 = x1;
            this.y1 = y1;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;

            Query query = (Query) o;

            if (x0 != query.x0) return false;
            if (y0 != query.y0) return false;
            if (x1 != query.x1) return false;
            return y1 == query.y1;
        }

        @Override
        public int hashCode() {
            int result = x0;
            result = 31 * result + y0;
            result = 31 * result + x1;
            result = 31 * result + y1;
            return result;
        }
    }
}
