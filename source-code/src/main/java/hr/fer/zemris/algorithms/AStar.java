package hr.fer.zemris.algorithms;

import hr.fer.zemris.graph.Edge;
import hr.fer.zemris.graph.Graph;
import hr.fer.zemris.graph.Node;

import java.util.*;
import java.util.stream.Collectors;

public class AStar implements PathfindingAlgorithm {
    @Override
    public List<Edge> getPath(int x1, int y1, int x2, int y2, Graph graph) {
        Set<Node> closedSet = new HashSet<>();
        Set<Node> openSet = new HashSet<>();
        Map<Node, Edge> cameFrom = new HashMap<>();
        Map<Node, Double> gScore = new HashMap<>();
        Map<Node, Double> fScore = new HashMap<>();

        graph.getNodes().forEach((location, node) -> {
            gScore.put(node, Double.MAX_VALUE);
            fScore.put(node, Double.MAX_VALUE);
        });

        Node source = graph.getNode(x1, y1);
        Node destination = graph.getNode(x2, y2);
        gScore.put(source, 0.0);
        fScore.put(source, heuristicDistance(source, destination));

        openSet.add(source);

        while (!openSet.isEmpty()) {
            Node current = minFromMapInSet(fScore, openSet);
            if (current.equals(destination)) {
                return reconstructPath(cameFrom, current);
            }
            openSet.remove(current);
            closedSet.add(current);

            for (Edge edge : current.getEdges()) {
                Node neighbour = edge.getTo();

                if (closedSet.contains(neighbour)) {
                    continue;
                }

                if (!openSet.contains(neighbour)) {
                    openSet.add(neighbour);
                }

                Double tentativeGScore = gScore.get(current) + edge.getWeight();
                if (tentativeGScore >= gScore.get(neighbour)) {
                    continue;
                }

                cameFrom.put(neighbour, edge);
                gScore.put(neighbour, tentativeGScore);
                fScore.put(neighbour, gScore.get(neighbour) + heuristicDistance(neighbour, destination));
            }
        }

        return new ArrayList<>();
    }

    private Node minFromMapInSet(Map<Node, Double> map, Set<Node> set) {
        return set.stream().sorted((o1, o2) -> Double.compare(map.get(o1), map.get(o2))).collect(Collectors.toList()).get(0);
    }

    private List<Edge> reconstructPath(Map<Node, Edge> cameFrom, Node current) {
        List<Edge> path = new ArrayList<>();
        while (cameFrom.containsKey(current)) {
            Edge lastEdge = cameFrom.get(current);
            path.add(lastEdge);
            current = lastEdge.getFrom();
        }
        Collections.reverse(path);
        return path;
    }

    public static double heuristicDistance(Node source, Node destination) {
        double dx = Math.abs(source.getLocation().x - destination.getLocation().x);
        double dy = Math.abs(source.getLocation().y - destination.getLocation().y);
        return Math.min(dx, dy) + Math.abs(dx - dy);
    }
}
