package hr.fer.zemris.algorithms;

import hr.fer.zemris.graph.Edge;
import hr.fer.zemris.graph.Graph;

import java.util.List;

public interface PathfindingAlgorithm {
    public List<Edge> getPath(int x1, int y1, int x2, int y2, Graph graph);
}
