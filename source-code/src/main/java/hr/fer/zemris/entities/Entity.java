package hr.fer.zemris.entities;

public interface Entity {
    public boolean canPassThrough();
}
