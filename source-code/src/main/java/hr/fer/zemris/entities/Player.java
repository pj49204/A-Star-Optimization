package hr.fer.zemris.entities;

import hr.fer.zemris.graph.Node;

import java.util.UUID;

public class Player {
    private String id;
    private Node.Location location;
    private double totalDistance;
    private double delta;
    private String name;

    public Player(Node.Location location) {
        this(location, 0);
    }

    public Player(Node.Location location, double totalDistance) {
        this.location = location;
        this.totalDistance = totalDistance;
        this.id = UUID.randomUUID().toString();
    }

    public Player(String id, Node.Location location, double totalDistance, double delta) {
        this.id = id;
        this.location = location;
        this.totalDistance = totalDistance;
        this.delta = delta;
    }

    public Node.Location getLocation() {
        return location;
    }

    public void setLocation(Node.Location location) {
        this.location = location;
    }

    public double getTotalDistance() {
        return totalDistance;
    }

    public void setTotalDistance(double totalDistance) {
        this.totalDistance = totalDistance;
    }

    public double getDelta() {
        return delta;
    }

    public void setDelta(double delta) {
        this.delta = delta;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Player player = (Player) o;

        return id != null ? id.equals(player.id) : player.id == null;
    }

    @Override
    public int hashCode() {
        return id != null ? id.hashCode() : 0;
    }

    @Override
    public String toString() {
        return "Player{" +
                "id='" + id + '\'' +
                ", location=" + location +
                ", totalDistance=" + totalDistance +
                ", delta=" + delta +
                ", name='" + name + '\'' +
                '}';
    }

    public Player getCopy() {
        Player player = new Player(id, location, totalDistance, delta);
        player.setName(name);
        return player;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }
}
