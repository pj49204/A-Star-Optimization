package hr.fer.zemris.graph;

public class Edge {
    private Node from;
    private Node to;
    private double weight;
    private double capacity;

    public Edge(Node from, Node to) {
        this(from, to, 1, 1);
    }

    public Edge(Node from, Node to, double weight, double capacity) {
        this.from = from;
        this.to = to;
        this.weight = weight;
        this.capacity = capacity;
    }

    public Node getFrom() {
        return from;
    }

    public void setFrom(Node from) {
        this.from = from;
    }

    public Node getTo() {
        return to;
    }

    public void setTo(Node to) {
        this.to = to;
    }

    public double getWeight() {
        return weight;
    }

    public void setWeight(double weight) {
        this.weight = weight;
    }

    public double getCapacity() {
        return capacity;
    }

    public void setCapacity(double capacity) {
        this.capacity = capacity;
    }

    @Override
    public String toString() {
        return "Edge{" +
                "from=" + from.toString() +
                ", to=" + to.toString() +
                '}';
    }
}
