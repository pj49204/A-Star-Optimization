package hr.fer.zemris.graph;

import com.jogamp.opengl.*;
import com.jogamp.opengl.awt.GLCanvas;
import com.jogamp.opengl.glu.GLU;
import hr.fer.zemris.algorithms.PathfindingAlgorithm;
import sun.nio.cs.UTF_32;

import javax.sound.sampled.Line;
import javax.swing.*;
import java.io.BufferedWriter;
import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.OpenOption;
import java.nio.file.StandardOpenOption;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Graph {

    Map<Node.Location, Node> nodes;

    public Graph(Map<Node.Location, Node> nodes) {
        this.nodes = nodes;
    }

    public Node getNode(int x, int y) {
        return nodes.get(new Node.Location(x, y));
    }

    public Map<Node.Location, Node> getNodes() {
        return nodes;
    }

    public double manhattanDistance(Node a, Node b) {
        return Math.abs(a.getLocation().x - b.getLocation().x) + Math.abs(a.getLocation().y - b.getLocation().y);
    }

    private List<Edge> getPath(int x1, int y1, int x2, int y2, PathfindingAlgorithm algorithm) {
        return algorithm.getPath(x1, y1, x2, y2, this);
    }

    public void visit(GraphVisitor visitor) {
        nodes.forEach((loc, node) -> visitor.visitNode(node));
    }

    public void writeToFile(File file) throws IOException {
        StringBuilder sb = new StringBuilder();
        Map<Node, Integer> nodeIntegerMap = new HashMap<>();
        final Integer[] i = {0};
        nodes.forEach((location, node) -> {
            nodeIntegerMap.put(node, i[0]);
            ++i[0];
        });
        sb.append("# nodes:").append(System.lineSeparator());
        nodes.forEach((location, node) -> {
            sb.append("v ").append(location.x).append(" ").append(location.y).append(" ").append(nodeIntegerMap.get(node)).append(System.lineSeparator());
        });
        sb.append("# edges:").append(System.lineSeparator());
        nodes.forEach((location, node) -> {
            node.getEdges().forEach(edge -> {
                sb.append("e ").append(nodeIntegerMap.get(edge.getFrom())).append(" ").append(nodeIntegerMap.get(edge.getTo())).append(" ").append("d ").append(edge.getWeight()).append(" ").append(edge.getCapacity()).append(System.lineSeparator());
            });
        });
        String toWrite = sb.toString().trim();
        System.out.println(toWrite);
        Files.write(file.toPath(), toWrite.getBytes(StandardCharsets.UTF_8));
    }

    public Graph getCopy() {
        Map<Node.Location, Node> newNodes = new HashMap<>();

        for (Map.Entry<Node.Location, Node> entry : nodes.entrySet()) {
            Node oldNode = entry.getValue();
            Node node = new Node(oldNode.getLocation().x, oldNode.getLocation().y);
            newNodes.put(entry.getKey(), node);
        }

        for (Map.Entry<Node.Location, Node> entry : newNodes.entrySet()) {
            Node node = entry.getValue();
            Node oldNode = nodes.get(node.getLocation());
            for (Edge edge : oldNode.getEdges()) {
                node.addEdge(new Edge(newNodes.get(edge.getFrom().getLocation()), newNodes.get(edge.getTo().getLocation()), edge.getWeight(), edge.getCapacity()));
            }
        }
        return new Graph(newNodes);
    }

}
