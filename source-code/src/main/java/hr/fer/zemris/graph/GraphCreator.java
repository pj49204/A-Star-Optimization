package hr.fer.zemris.graph;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.*;

public class GraphCreator {
    public static Graph parseFromFile(File file) throws IOException {
        Scanner scanner = new Scanner(Files.newInputStream(file.toPath()));
        Map<Integer, Node> nodes = new HashMap<>();
        while (scanner.hasNextLine()) {
            String line = scanner.nextLine();
            line = line.trim().toLowerCase();
            switch (line.charAt(0)) {
                case '#':
                    break;
                case 'v':
                    String[] parts = line.split(" ");
                    int x = Integer.parseInt(parts[1]);
                    int y = Integer.parseInt(parts[2]);
                    int id = Integer.parseInt(parts[3]);
                    Node node = new Node(x, y);
                    nodes.put(id, node);
                    break;
                case 'e':
                    String[] edgeParts = line.split(" ");
                    int id1 = Integer.parseInt(edgeParts[1]);
                    int id2 = Integer.parseInt(edgeParts[2]);
                    boolean undirected = edgeParts[3].equals("u");
                    double weight = Double.parseDouble(edgeParts[4]);
                    double capacity = Double.parseDouble(edgeParts[5]);
                    Node from = nodes.get(id1);
                    Node to = nodes.get(id2);
                    Edge fromToEdge = new Edge(from, to, weight, capacity);
                    from.addEdge(fromToEdge);
                    if (undirected) {
                        Edge toFromEdge = new Edge(to, from);
                        to.addEdge(toFromEdge);
                    }
                    break;
                default:
                    break;
            }
        }
        Map<Node.Location, Node> nodeMap = new HashMap<>();
        nodes.forEach((integer, node) -> {
            nodeMap.put(node.getLocation(), node);
        });
        return new Graph(nodeMap);
    }
}
