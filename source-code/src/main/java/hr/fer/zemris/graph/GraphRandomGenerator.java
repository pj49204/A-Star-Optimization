package hr.fer.zemris.graph;

import java.security.SecureRandom;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;

public class GraphRandomGenerator {

    private static final double NODE_CHANCE = 0.9;
    private static final double EDGE_CHANCE = 0.3;
    private static final Random random = new SecureRandom();

    public static Graph generateRandomGraph(int height, int width) {
        Map<Node.Location, Node> locationNodeMap = new HashMap<>();

        for (int i = 0; i < height; i++) {
            for (int j = 0; j < width; j++) {
                if (random.nextDouble() < NODE_CHANCE) {
                    Node.Location location = new Node.Location(i, j);
                    Node node = new Node(i, j);
                    locationNodeMap.put(location, node);
                }
            }
        }

        locationNodeMap.forEach((location, node) -> {
            for (int i = -1; i <= 1; i++) {
                for (int j = -1; j <= 1; j++) {
                    if (i == 0 && j == 0) {
                        continue;
                    }
                    Node.Location newLocation = new Node.Location(location.x + i, location.y + j);
                    if (isValid(newLocation.x, newLocation.y, width, height) && locationNodeMap.containsKey(newLocation)) {
                        Node to = locationNodeMap.get(newLocation);
                        final boolean[] added = {false};
                        to.getEdges().forEach(edge -> {
                            if (edge.getTo().equals(to)){
                                added[0] = true;
                            }
                        });
                        if(added[0]){
                            continue;
                        }
                        if (random.nextDouble() < EDGE_CHANCE) {
                            node.addEdge(new Edge(node, to));
                            node.addEdge(new Edge(to, node));
                        }
                    }
                }
            }
        });

        return new Graph(locationNodeMap);
    }

    private static boolean isValid(int x, int y, int width, int height) {
        return x >= 0 && y >= 0 && x < width && y < height;
    }
}
