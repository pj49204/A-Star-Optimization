package hr.fer.zemris.graph;

public interface GraphVisitor {
    public void visitNode(Node node);
}
