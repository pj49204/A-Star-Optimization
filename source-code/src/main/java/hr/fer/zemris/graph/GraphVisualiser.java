package hr.fer.zemris.graph;

import com.jogamp.opengl.*;
import com.jogamp.opengl.awt.GLCanvas;

import javax.swing.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class GraphVisualiser {

    private static final float BACKGROUND  = 0.0f;
    private static final float FOREGROUND  = 0.9f;

    private final GLCanvas glcanvas = new GLCanvas(new GLCapabilities(GLProfile.get(GLProfile.GL2ES1)));

    private Graph graph;
    private double maxX;
    private double maxY;
    private List<GLEventListener> listeners = new ArrayList<>();

    public GraphVisualiser(Graph graph) {
        final double[] maxX = {Double.MIN_VALUE};
        final double[] maxY = {Double.MIN_VALUE};
        this.graph = graph;
        graph.getNodes().forEach((location, node) -> {
            maxX[0] = Double.max(maxX[0], location.x);
            maxY[0] = Double.max(maxY[0], location.x);
        });
        this.maxX = maxX[0];
        this.maxY = maxY[0];
    }

    public void visualize() {

        // The canvas

        glcanvas.addGLEventListener(new GLEventListener() {
            @Override
            public void init(GLAutoDrawable drawable) {
                GL gl = drawable.getGL();
                gl.glClear(GL.GL_COLOR_BUFFER_BIT | GL.GL_DEPTH_BUFFER_BIT);
                drawable.getGL().glClearColor(BACKGROUND, BACKGROUND, BACKGROUND, 0.0f);
            }

            @Override
            public void dispose(GLAutoDrawable drawable) {

            }

            @Override
            public void display(GLAutoDrawable drawable) {
                GL gl = drawable.getGL();
                gl.glClear(GL.GL_COLOR_BUFFER_BIT | GL.GL_DEPTH_BUFFER_BIT);
                drawable.getGL().glClearColor(BACKGROUND, BACKGROUND, BACKGROUND, 0.0f);
            }

            @Override
            public void reshape(GLAutoDrawable drawable, int x, int y, int width, int height) {

            }
        });

        graph.getNodes().forEach((location, node) -> {
            GLEventListener listenerDot = new Dot(location.x, location.y);
            glcanvas.addGLEventListener(listenerDot);
            node.getEdges().forEach(edge -> {
                Node.Location from = edge.getFrom().getLocation();
                Node.Location to = edge.getTo().getLocation();
                GLEventListener listener = new Line(from.x, from.y, to.x, to.y);
                glcanvas.addGLEventListener(listener);
            });
        });


        glcanvas.setSize(1000, 1000);


        //creating frame
        final JFrame frame = new JFrame("Random graph");
//
//        final JPanel panel = new JPanel();
//        frame.add(panel);
        //adding canvas to frame
        frame.add(glcanvas);

        frame.setSize(frame.getContentPane().getPreferredSize());
        frame.setVisible(true);

        frame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
    }

    private class Line implements GLEventListener {
        int x0;
        int y0;
        int x1;
        int y1;
        double red;
        double blue;
        double green;

        public Line(int x0, int y0, int x1, int y1) {
            this(x0, y0, x1, y1, FOREGROUND, FOREGROUND, FOREGROUND);
        }

        public Line(int x0, int y0, int x1, int y1, double red, double blue, double green) {
            this.x0 = x0;
            this.y0 = y0;
            this.x1 = x1;
            this.y1 = y1;
            this.red = red;
            this.blue = blue;
            this.green = green;
        }

        @Override
        public void init(GLAutoDrawable glAutoDrawable) {

        }

        @Override
        public void dispose(GLAutoDrawable glAutoDrawable) {

        }

        @Override
        public void display(GLAutoDrawable glAutoDrawable) {
            final GL2 gl = glAutoDrawable.getGL().getGL2();
            gl.glBegin(GL2.GL_LINES);
            float[] currentColor = new float[4];
            gl.glColor3d(red, green, blue);
            gl.glVertex2d(0.9 * x0 / (GraphVisualiser.this.maxX / 2.0) - 0.9, 0.9 * y0 / (GraphVisualiser.this.maxY / 2.0) - 0.9);
            gl.glVertex2d(0.9 * x1 / (GraphVisualiser.this.maxX / 2.0) - 0.9, 0.9 * y1 / (GraphVisualiser.this.maxY / 2.0) - 0.9);
            gl.glEnd();
        }

        @Override
        public void reshape(GLAutoDrawable glAutoDrawable, int i, int i1, int i2, int i3) {

        }
    }

    private class Dot implements GLEventListener {
        int x;
        int y;

        public Dot(int x, int y) {
            this.x = x;
            this.y = y;
        }

        @Override
        public void init(GLAutoDrawable drawable) {

        }

        @Override
        public void dispose(GLAutoDrawable drawable) {

        }

        @Override
        public void display(GLAutoDrawable drawable) {
            final GL2 gl = drawable.getGL().getGL2();
            gl.glBegin(GL2.GL_POINTS);
            gl.glPointSize(20f);
            gl.glColor3d(1.0, 0.0, 0.0);
            gl.glVertex2d(0.9 * x / (GraphVisualiser.this.maxX / 2.0) - 0.9, 0.9 * y / (GraphVisualiser.this.maxY / 2.0) - 0.9);
            gl.glColor3d(1.0, 1.0, 1.0);
            gl.glPointSize(1);
            gl.glEnd();

        }

        @Override
        public void reshape(GLAutoDrawable drawable, int x, int y, int width, int height) {

        }
    }

    public void visualisePath(List<Edge> edges) {
        Random random = new Random(new Random().nextInt());
        double r = (random.nextDouble() + 0.7) / 2.0;
        double b = (random.nextDouble() + 0.7) / 2.0;
        double g = 0;
        for (Edge edge : edges) {
            Node.Location from = edge.getFrom().getLocation();
            Node.Location to = edge.getTo().getLocation();
            GLEventListener listener = new Line(from.x, from.y, to.x, to.y, r, b, g);
            glcanvas.addGLEventListener(listener);
            this.listeners.add(listener);
        }
    }

    public void refresh() {
        glcanvas.repaint();
    }

    public void removeAllPaths() {
        for (GLEventListener listener : listeners) {
            glcanvas.removeGLEventListener(listener);
        }
        listeners = new ArrayList<>();
        refresh();
    }
}
