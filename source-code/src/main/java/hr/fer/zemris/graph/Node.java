package hr.fer.zemris.graph;

import hr.fer.zemris.entities.Entity;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.util.function.BiFunction;

public class Node implements Comparable<Node> {
    private List<Entity> entities;
    private List<Edge> edges;
    private Location location;

    public Node(int x, int y) {
        this(new ArrayList<Entity>(), new ArrayList<Edge>(), x, y);
    }

    public Node(List<Edge> edges, int x, int y) {
        this(new ArrayList<Entity>(), edges, x, y);
    }

    public Node(List<Entity> entities, List<Edge> edges, int x, int y) {
        this.entities = entities;
        this.edges = edges;
        this.location = new Location(x,y);
    }

    public void addEntity(Entity ... entities) {
        this.entities.addAll(Arrays.asList(entities));
    }

    public void removeEntity(Entity entity) {
        this.entities.remove(entity);
    }

    public List<Entity> getEntities() {
        return entities;
    }

    public void setEntities(List<Entity> entities) {
        this.entities = entities;
    }

    public List<Edge> getEdges() {
        return edges;
    }

    public void setEdges(List<Edge> edges) {
        this.edges = edges;
    }

    public void addEdge(Edge edge){
        edges.add(edge);
    }

    public Location getLocation() {
        return location;
    }

    public void setLocation(Location location) {
        this.location = location;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Node node = (Node) o;

        return location.equals(node.location);
    }

    @Override
    public int hashCode() {
        return location.hashCode();
    }

    @Override
    public int compareTo(Node o) {
        return location.compareTo(o.location);
    }

    @Override
    public String toString() {
        return "Node{" +
                "location=" + location.toString() +
                '}';
    }

    public static class Location implements Comparable<Location> {
        public int x;
        public int y;

        public Location(int x, int y) {
            this.x = x;
            this.y = y;
        }

        public int compareTo(Location o) {
            return x != o.x ? Integer.compare(x, o.x) : Integer.compare(y, o.y);
        }

        @Override
        public String toString() {
            return "Location{" +
                    "x=" + x +
                    ", y=" + y +
                    '}';
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;

            Location location = (Location) o;

            return x == location.x && y == location.y;
        }

        @Override
        public int hashCode() {
            int result = x;
            result = 31 * result + y;
            return result;
        }
    }
}
