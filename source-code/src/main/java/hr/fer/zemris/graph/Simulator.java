package hr.fer.zemris.graph;

import hr.fer.zemris.algorithms.ACOAlgorithm;
import hr.fer.zemris.algorithms.AStar;
import hr.fer.zemris.algorithms.PathfindingAlgorithm;
import hr.fer.zemris.entities.Player;

import java.io.File;
import java.io.IOException;
import java.util.*;

public class Simulator {
    private Graph graph;
    private List<Player> players;
    private Map<Player, List<Edge>> paths;
    private Map<Player, Node.Location> previousLocation;

    public Simulator(Graph graph) {
        this(graph, new HashMap<>());
    }

    public Simulator(Graph graph, Map<Player, List<Edge>> paths) {
        this(graph, new ArrayList<>(paths.keySet()), paths);
    }

    public Simulator(Graph graph, List<Player> players, Map<Player, List<Edge>> paths) {
        this.graph = graph;
        this.players = players;
        this.paths = paths;
        previousLocation = new HashMap<>();
    }

    public void addPath(Player player, List<Edge> path) {
        if (!paths.containsKey(player)) {
            players.add(player);
        }
        paths.put(player, path);
    }

    public List<Player> getPlayers() {
        return players;
    }

    public Graph getGraph() {
        return graph;
    }

    public void setGraph(Graph graph) {
        this.graph = graph;
    }

    public Map<Player, List<Edge>> getPaths() {
        return paths;
    }

    public void step() throws NoMoreStepsException {
        boolean noSteps = true;
        List<Edge> edgesToUpdate = new ArrayList<>();
        for (Player player : getPlayers()) {
            List<Edge> path = paths.get(player);
            if (path.isEmpty()) {
                continue;
            }
            if (!player.getLocation().equals(previousLocation.get(player))) {
                path.get(0).setCapacity(Math.max(path.get(0).getCapacity() * 0.9, 0.001));
            }
        }
        for (Player player : players) {
            List<Edge> path = paths.get(player);
            if (path.isEmpty()) {
                continue;
            }
            noSteps = false;
            Edge edge = path.get(0);
//            System.out.println(player.getId());
            double weight = edge.getWeight() * edge.getCapacity();
//            System.out.printf("Weight: %f\n", weight);
            player.setTotalDistance(player.getTotalDistance() + 1);
            previousLocation.put(player, player.getLocation());
//            System.out.printf("Total distance: %f\n", player.getTotalDistance());
//            System.out.printf("Delta: %f\n", player.getDelta());
//            System.out.printf("Edge weight: %f\n", edge.getWeight());
            double overflow = player.getDelta() + weight - edge.getWeight();
            if (overflow >= -1e-5) {
                edge.setCapacity(Math.min(edge.getCapacity() / 0.9, 1.0));
                edgesToUpdate.add(path.get(0));
                Node dst = edge.getTo();
                player.setLocation(dst.getLocation());
                path.remove(0);
                if (!path.isEmpty()) {
                    player.setDelta(overflow * path.get(0).getCapacity());
                } else {
                    player.setDelta(overflow);
//                    System.out.println(player.getTotalDistance());
                }
            } else {
                player.setDelta(weight + player.getDelta());
            }
//            System.out.println(entry.getKey().getLocation());
        }
        for (Edge edge : edgesToUpdate) {
            edge.setCapacity(Math.min(edge.getCapacity() / 0.9, 1.0));
        }
        if (noSteps) {
            throw new NoMoreStepsException("No more moves available");
        }
    }

    public Simulator getCopy() {
        Graph graph = this.graph.getCopy();
        Map<Player, List<Edge>> paths = new HashMap<>();
        List<Player> players = new ArrayList<>();
        for (Player player : this.players) {
            List<Edge> path = new ArrayList<>();
            for (Edge edge : this.paths.get(player)) {
                for (Edge edge1 : graph.getNodes().get(edge.getFrom().getLocation()).getEdges()) {
                    if (edge1.getTo().equals(edge.getTo())) {
                        path.add(edge1);
                        break;
                    }
                }
            }
            Player playerCopy = player.getCopy();
            paths.put(playerCopy, path);
//            playerCopy.setDelta(0.0);
            players.add(playerCopy);
        }
        return new Simulator(graph, players, paths);
    }

    public void removeAllPaths() {
        paths = new HashMap<>();
        players = new ArrayList<>();
        previousLocation = new HashMap<>();
        for (Map.Entry<Node.Location, Node> entry : graph.getNodes().entrySet()) {
            for (Edge edge : entry.getValue().getEdges()) {
                edge.setCapacity(1.0);
            }
        }
    }

    public static class NoMoreStepsException extends RuntimeException {
        public NoMoreStepsException() {
        }

        public NoMoreStepsException(String message) {
            super(message);
        }

        public NoMoreStepsException(String message, Throwable cause) {
            super(message, cause);
        }

        public NoMoreStepsException(Throwable cause) {
            super(cause);
        }

        public NoMoreStepsException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
            super(message, cause, enableSuppression, writableStackTrace);
        }
    }
}
