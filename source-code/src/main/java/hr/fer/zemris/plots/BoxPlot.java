package hr.fer.zemris.plots;

import java.awt.Dimension;
import java.awt.EventQueue;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import javax.swing.JFrame;
import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.data.statistics.DefaultBoxAndWhiskerCategoryDataset;

public class BoxPlot {

    private static final String ROW_KEY = "";

    private List<Number> inputData;

    public BoxPlot(List<Number> inputData) {
        super();
        this.inputData = inputData;
    }

    public void display() {
        JFrame f = new JFrame("BoxPlot");
        f.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        DefaultBoxAndWhiskerCategoryDataset data = new DefaultBoxAndWhiskerCategoryDataset();
        data.add(inputData, ROW_KEY, "");
        JFreeChart chart = ChartFactory.createBoxAndWhiskerChart(
            "Box plot", ROW_KEY, "Ticks", data, false);
        f.add(new ChartPanel(chart) {
            @Override
            public Dimension getPreferredSize() {
                return new Dimension(320, 480);
            }
        });
        f.pack();
        f.setLocationRelativeTo(null);
        f.setVisible(true);
    }
}